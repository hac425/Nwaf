service rsyslog restart
/usr/local/openresty/nginx/sbin/nginx -p /waf/nginx_work/ -c conf/nginx.conf
chmod -R 777 /waf/logs/
service cron restart
cd /waf/code/Nwaf-web
# wait for database
python init_waf.py
python app.py
