FROM ubuntu:16.04

ADD sources.list /etc/apt/

RUN apt-get update; \
        apt-get install -y wget curl;

RUN apt-get -y install python2.7 make
RUN apt-get install -y libreadline-dev libncurses5-dev libpcre3-dev libssl-dev perl make build-essential
RUN apt-get -y install python-pip


ADD pip.conf /etc/


RUN mkdir /waf
ADD openresty-1.13.6.2.tar.gz /waf
ADD requirements.txt /waf


WORKDIR /waf/
RUN pip install -r requirements.txt

RUN cd openresty-1.13.6.2 && ./configure && make && make install


RUN mkdir /waf/nginx_work/ && mkdir /waf/nginx_work/logs && mkdir /waf/nginx_work/vhosts && mkdir /waf/nginx_work/conf && mkdir logs && chmod 777 logs

COPY ./waf_lua/conf/nginx.conf /waf/nginx_work/conf/nginx.conf


RUN apt-get -y install rsyslog cron tzdata
# 为了搜集数据，不改时区了， 因为寝室熄灯 emmm
# RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

COPY crontab /etc/crontab
RUN chmod 644 /etc/crontab